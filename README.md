# babel-everything
A Node package that depends on all Babel modules.

## Why?
I got tired of installing 50 Babel modules one by one.
